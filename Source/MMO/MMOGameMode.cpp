// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MMOGameMode.h"
#include "MMOHud.h"
#include "Kismet/GameplayStatics.h"
#include "MMOSaveGame.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "Runtime/Engine/Classes/Engine/World.h"

AMMOGameMode::AMMOGameMode()
{
	DefaultPawnClass = AMMOPawn::StaticClass();
	HUDClass = AMMOHud::StaticClass();
	http = &FHttpModule::Get();

	PrimaryActorTick.TickInterval = .1f;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

}

void AMMOGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Get main player pawn
	vehiclePawn = Cast<AMMOPawn>(GetWorld()->GetFirstPlayerController()->GetPawn());

	getSlot();

	// Check if player has UID
	//vehiclePawn->pUID = this->loadSavedUID();
	//if (vehiclePawn->pUID == 0) {
	//	vehiclePawn->isActive = false;
	//	downloadUID();
	//}
	//else {
	//	vehiclePawn->isActive = true;
	//}

	// downloadActivePlayers(vehiclePawn->slotID);
}

void AMMOGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	downloadActivePlayers(vehiclePawn->slotID);
}

// load pUID from memory
int32 AMMOGameMode::loadSavedUID()
{

	int32 returnVar = 0;

	try {
		UMMOSaveGame* saveGameInstance = Cast<UMMOSaveGame>(UGameplayStatics::CreateSaveGameObject(UMMOSaveGame::StaticClass()));
		saveGameInstance = Cast<UMMOSaveGame>(UGameplayStatics::LoadGameFromSlot(saveGameInstance->slotName, saveGameInstance->userIndex));

		if (saveGameInstance) {
			returnVar = saveGameInstance->savedUID;
		}
	}
	catch (...) {
	}

	return returnVar;
}

void AMMOGameMode::getSlot()
{
	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AMMOGameMode::onSlotReceived);
	Request->SetURL("http://192.168.1.2/unreal/getSlot.php");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

// TODO: Lock row and confirm slot
void AMMOGameMode::onSlotReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (bWasSuccessful) {
		try {

			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject;

			//Create a reader pointer to read the json data
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

			//Deserialize the json data given Reader and the actual object to deserialize
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				int32 receivedSlot = JsonObject->GetNumberField("SlotID");
				int32 receivedPlayerUID = JsonObject->GetNumberField("PlayerUID");
				int32 receivedMaterialIndex = JsonObject->GetNumberField("Material");

				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Received Slot: ") + FString::FromInt(receivedSlot));
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Received Player UID: ") + FString::FromInt(receivedPlayerUID));

				if (receivedSlot != 0) {
					vehiclePawn->slotID = receivedSlot;
					vehiclePawn->pUID = receivedPlayerUID;
					vehiclePawn->materialIndex = receivedMaterialIndex;
					vehiclePawn->isActive = true;

					vehiclePawn->loadMaterial();
				}
				else {
					GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Slots full"));
				}
			}
		}
		catch (...) {
			// Show debug error
		}
	}
}

void AMMOGameMode::downloadUID()
{
	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AMMOGameMode::onUIDReceived);
	Request->SetURL("http://192.168.1.2/unreal/getUID.php");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

void AMMOGameMode::onUIDReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (bWasSuccessful) {
		try {

			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject;

			//Create a reader pointer to read the json data
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

			//Deserialize the json data given Reader and the actual object to deserialize
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				int32 receivedUID = JsonObject->GetNumberField("playerUID");
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Received UID: ") + FString::FromInt(receivedUID));

				confirmUID(receivedUID);

			}
		}
		catch (...) {
			// Show debug error
		}
	}
}

void AMMOGameMode::confirmUID(int32 UID)
{
	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AMMOGameMode::onUIDConfirmed);
	Request->SetURL("http://192.168.1.2/unreal/saveUID.php");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	FString json = "{ \"playerUID\" : " + FString::FromInt(UID) + "}";
	Request->SetContentAsString(json);
	Request->ProcessRequest();
}

void AMMOGameMode::onUIDConfirmed(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (bWasSuccessful) {
		try {

			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject;

			//Create a reader pointer to read the json data
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

			//Deserialize the json data given Reader and the actual object to deserialize
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				int32 confirmedUID = JsonObject->GetNumberField("playerUID");
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Confirmed pUID: ") + FString::FromInt(vehiclePawn->pUID));
				saveUID(confirmedUID);
			}
		}
		catch (...) {
			// Show debug error
		}
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Request failed!"));
	}
}

void AMMOGameMode::saveUID(int32 UID)
{
	try {
		// Save user info
		UMMOSaveGame* saveGameInstance = Cast<UMMOSaveGame>(UGameplayStatics::CreateSaveGameObject(UMMOSaveGame::StaticClass()));
		saveGameInstance->savedUID = UID;

		// .sav file to be same as UID though that would beat the logic of hiding player's UID in the save file
		UGameplayStatics::SaveGameToSlot(saveGameInstance, saveGameInstance->slotName, saveGameInstance->userIndex);

		// Use events/triggers instead
		vehiclePawn->pUID = UID;
		vehiclePawn->isActive = true;
	}
	catch (...) {
	}
}

// Exclude currently possesed player
void AMMOGameMode::downloadActivePlayers(int32 ID)
{
	// GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Download active players!"));


	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AMMOGameMode::onActivePlayersReceived);
	Request->SetURL("http://192.168.1.2/unreal/getActivePlayers.php");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	FString json = "{ \"slotID\" : " + FString::FromInt(ID) + "}";
	Request->SetContentAsString(json);
	Request->ProcessRequest();
}

void AMMOGameMode::onActivePlayersReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (bWasSuccessful) {

		try {

			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject;

			//Create a reader pointer to read the json data
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

			//Deserialize the json data given Reader and the actual object to deserialize
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{
				TArray <TSharedPtr<FJsonValue>> activePlayersArr = JsonObject->GetArrayField("activeplayers");
				TMap<int32, TTuple<int32, int32>> downloadedPlayers;

				for (auto Elem : activePlayersArr) {
					TSharedPtr<FJsonObject> playerData = Elem->AsObject();
					int32 slotID = playerData->GetIntegerField("SlotID");
					int32 playerUID = playerData->GetIntegerField("PlayerUID");
					int32 materialIndex = playerData->GetIntegerField("Material");

					downloadedPlayers.Add(playerUID, TTuple<int32, int32>(slotID, materialIndex));
				}

				processPlayers(downloadedPlayers);

				numberOfActivePlayers = JsonObject->GetIntegerField("size");
			}
		}
		catch (...) {
			// Show debug error
		}
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Failed to get active players!"));
	}
}


// Find complement/intersection add new players remove old players
void AMMOGameMode::processPlayers(TMap<int32, TTuple<int32, int32>> map)
{
	TSet<int32> newPlayers;


	// find new players
	for (auto newPlayer : map) {
		if (!players.Contains(newPlayer.Key)) {
			this->addPlayer(newPlayer.Value.Key, newPlayer.Value.Value, newPlayer.Key, false, true);
		}
	}

	// remove old players
	for (auto oldPlayer : players) {
		if (!map.Contains(oldPlayer.Key)) {
			AMMOPawn* player = players.FindChecked(oldPlayer.Key);
			players.Remove(oldPlayer.Key);
			player->Destroy();

			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString::FromInt(oldPlayer.Key) + TEXT(" : Removed!"));

			//removedPlayer->K2
		}
	}
}

// TODO: Instantiate first before spawning
AMMOPawn* AMMOGameMode::addPlayer(int32 slotID, int32 materialIndex, int32 playerUID, bool isActive, bool isOtherPlayer)
{
	FActorSpawnParameters spawnInfo;

	//Didn't show actor on scene without this line below
	spawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AMMOPawn* newActor = GetWorld()->SpawnActor<AMMOPawn>(AMMOPawn::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, spawnInfo);
	newActor->isActive = isActive;
	newActor->slotID = slotID;
	newActor->pUID = playerUID;
	newActor->materialIndex = materialIndex;
	newActor->isOtherPlayer = isOtherPlayer;

	if (newActor) {
		players.Add(playerUID, newActor);
		newActor->loadMaterial();
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, TEXT("New player added!"));
	}

	return newActor;
}
