// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MMOPawn.h"
#include "MMOWheelFront.h"
#include "MMOWheelRear.h"
#include "MMOHud.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "Engine/SkeletalMesh.h"
#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/TextRenderComponent.h"
#include "Materials/Material.h"
#include "GameFramework/Controller.h"
// Added
#include "Materials/MaterialInstanceDynamic.h"

// Needed for VR Headset
#if HMD_MODULE_INCLUDED
#include "IHeadMountedDisplay.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#endif // HMD_MODULE_INCLUDED

const FName AMMOPawn::LookUpBinding("LookUp");
const FName AMMOPawn::LookRightBinding("LookRight");

#define LOCTEXT_NAMESPACE "VehiclePawn"

AMMOPawn::AMMOPawn()
{
	// Car mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> CarMesh(TEXT("/Game/Vehicle/Sedan/Sedan_SkelMesh.Sedan_SkelMesh"));
	GetMesh()->SetSkeletalMesh(CarMesh.Object);

	static ConstructorHelpers::FClassFinder<UObject> AnimBPClass(TEXT("/Game/Vehicle/Sedan/Sedan_AnimBP"));
	GetMesh()->SetAnimInstanceClass(AnimBPClass.Class);

	// Simulation
	UWheeledVehicleMovementComponent4W* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent4W>(GetVehicleMovement());

	check(Vehicle4W->WheelSetups.Num() == 4);

	Vehicle4W->WheelSetups[0].WheelClass = UMMOWheelFront::StaticClass();
	Vehicle4W->WheelSetups[0].BoneName = FName("Wheel_Front_Left");
	Vehicle4W->WheelSetups[0].AdditionalOffset = FVector(0.f, -12.f, 0.f);

	Vehicle4W->WheelSetups[1].WheelClass = UMMOWheelFront::StaticClass();
	Vehicle4W->WheelSetups[1].BoneName = FName("Wheel_Front_Right");
	Vehicle4W->WheelSetups[1].AdditionalOffset = FVector(0.f, 12.f, 0.f);

	Vehicle4W->WheelSetups[2].WheelClass = UMMOWheelRear::StaticClass();
	Vehicle4W->WheelSetups[2].BoneName = FName("Wheel_Rear_Left");
	Vehicle4W->WheelSetups[2].AdditionalOffset = FVector(0.f, -12.f, 0.f);

	Vehicle4W->WheelSetups[3].WheelClass = UMMOWheelRear::StaticClass();
	Vehicle4W->WheelSetups[3].BoneName = FName("Wheel_Rear_Right");
	Vehicle4W->WheelSetups[3].AdditionalOffset = FVector(0.f, 12.f, 0.f);

	// Create a spring arm component
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	SpringArm->TargetOffset = FVector(0.f, 0.f, 200.f);
	SpringArm->SetRelativeRotation(FRotator(-15.f, 0.f, 0.f));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 600.0f;
	SpringArm->bEnableCameraRotationLag = true;
	SpringArm->CameraRotationLagSpeed = 7.f;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;

	// Create camera component 
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;
	Camera->FieldOfView = 90.f;

	// Create In-Car camera component 
	InternalCameraOrigin = FVector(0.0f, -40.0f, 120.0f);

	InternalCameraBase = CreateDefaultSubobject<USceneComponent>(TEXT("InternalCameraBase"));
	InternalCameraBase->SetRelativeLocation(InternalCameraOrigin);
	InternalCameraBase->SetupAttachment(GetMesh());

	InternalCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("InternalCamera"));
	InternalCamera->bUsePawnControlRotation = false;
	InternalCamera->FieldOfView = 90.f;
	InternalCamera->SetupAttachment(InternalCameraBase);

	//Setup TextRenderMaterial
	static ConstructorHelpers::FObjectFinder<UMaterial> TextMaterial(TEXT("Material'/Engine/EngineMaterials/AntiAliasedTextMaterialTranslucent.AntiAliasedTextMaterialTranslucent'"));

	UMaterialInterface* Material = TextMaterial.Object;

	// Create text render component for in car speed display
	InCarSpeed = CreateDefaultSubobject<UTextRenderComponent>(TEXT("IncarSpeed"));
	InCarSpeed->SetTextMaterial(Material);
	InCarSpeed->SetRelativeLocation(FVector(70.0f, -75.0f, 99.0f));
	InCarSpeed->SetRelativeRotation(FRotator(18.0f, 180.0f, 0.0f));
	InCarSpeed->SetupAttachment(GetMesh());
	InCarSpeed->SetRelativeScale3D(FVector(1.0f, 0.4f, 0.4f));

	// Create text render component for in car gear display
	InCarGear = CreateDefaultSubobject<UTextRenderComponent>(TEXT("IncarGear"));
	InCarGear->SetTextMaterial(Material);
	InCarGear->SetRelativeLocation(FVector(66.0f, -9.0f, 95.0f));
	InCarGear->SetRelativeRotation(FRotator(25.0f, 180.0f, 0.0f));
	InCarGear->SetRelativeScale3D(FVector(1.0f, 0.4f, 0.4f));
	InCarGear->SetupAttachment(GetMesh());

	// Colors for the incar gear display. One for normal one for reverse
	GearDisplayReverseColor = FColor(255, 0, 0, 255);
	GearDisplayColor = FColor(255, 255, 255, 255);

	// Colors for the in-car gear display. One for normal one for reverse
	GearDisplayReverseColor = FColor(255, 0, 0, 255);
	GearDisplayColor = FColor(255, 255, 255, 255);

	bInReverseGear = false;

	// Added to function from here below

	http = &FHttpModule::Get();




}

void AMMOPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMMOPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMMOPawn::MoveRight);
	PlayerInputComponent->BindAxis("LookUp");
	PlayerInputComponent->BindAxis("LookRight");

	PlayerInputComponent->BindAction("Handbrake", IE_Pressed, this, &AMMOPawn::OnHandbrakePressed);
	PlayerInputComponent->BindAction("Handbrake", IE_Released, this, &AMMOPawn::OnHandbrakeReleased);
	PlayerInputComponent->BindAction("SwitchCamera", IE_Pressed, this, &AMMOPawn::OnToggleCamera);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AMMOPawn::OnResetVR);
}

void AMMOPawn::MoveForward(float Val)
{
	GetVehicleMovementComponent()->SetThrottleInput(Val);
}

void AMMOPawn::MoveRight(float Val)
{
	GetVehicleMovementComponent()->SetSteeringInput(Val);
}

void AMMOPawn::OnHandbrakePressed()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(true);
}

void AMMOPawn::OnHandbrakeReleased()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(false);
}

void AMMOPawn::OnToggleCamera()
{
	EnableIncarView(!bInCarCameraActive);
}

void AMMOPawn::EnableIncarView(const bool bState, const bool bForce)
{
	if ((bState != bInCarCameraActive) || (bForce == true))
	{
		bInCarCameraActive = bState;

		if (bState == true)
		{
			OnResetVR();
			Camera->Deactivate();
			InternalCamera->Activate();
		}
		else
		{
			InternalCamera->Deactivate();
			Camera->Activate();
		}

		InCarSpeed->SetVisibility(bInCarCameraActive);
		InCarGear->SetVisibility(bInCarCameraActive);
	}
}


void AMMOPawn::Tick(float Delta)
{
	Super::Tick(Delta);

	// Setup the flag to say we are in reverse gear
	bInReverseGear = GetVehicleMovement()->GetCurrentGear() < 0;

	// Update the strings used in the hud (incar and onscreen)
	UpdateHUDStrings();

	// Set the string in the incar hud
	SetupInCarHUD();

	bool bHMDActive = false;
#if HMD_MODULE_INCLUDED
	if ((GEngine->HMDDevice.IsValid() == true) && ((GEngine->HMDDevice->IsHeadTrackingAllowed() == true) || (GEngine->IsStereoscopic3D() == true)))
	{
		bHMDActive = true;
	}
#endif // HMD_MODULE_INCLUDED
	if (bHMDActive == false)
	{
		if ((InputComponent) && (bInCarCameraActive == true))
		{
			FRotator HeadRotation = InternalCamera->RelativeRotation;
			HeadRotation.Pitch += InputComponent->GetAxisValue(LookUpBinding);
			HeadRotation.Yaw += InputComponent->GetAxisValue(LookRightBinding);
			InternalCamera->RelativeRotation = HeadRotation;
		}
	}

	if (isActive) {
		postDataHTTP();
	}

	if (isOtherPlayer) {
		getDataHTTP(this->slotID);
	}
}

void AMMOPawn::BeginPlay()
{
	Super::BeginPlay();

	bool bEnableInCar = false;
#if HMD_MODULE_INCLUDED
	bEnableInCar = UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled();
#endif // HMD_MODULE_INCLUDED
	EnableIncarView(bEnableInCar, true);

	PrimaryActorTick.TickInterval = .1f;

	this->SetActorLocation(FVector(GetActorLocation().X + randomFloat(-3000, 3000), GetActorLocation().Y + randomFloat(-3000, 3000), GetActorLocation().Z), false, NULL, ETeleportType::TeleportPhysics);


}

void AMMOPawn::OnResetVR()
{
#if HMD_MODULE_INCLUDED
	if (GEngine->HMDDevice.IsValid())
	{
		GEngine->HMDDevice->ResetOrientationAndPosition();
		InternalCamera->SetRelativeLocation(InternalCameraOrigin);
		GetController()->SetControlRotation(FRotator());
	}
#endif // HMD_MODULE_INCLUDED
}

void AMMOPawn::UpdateHUDStrings()
{
	float KPH = FMath::Abs(GetVehicleMovement()->GetForwardSpeed()) * 0.036f;
	int32 KPH_int = FMath::FloorToInt(KPH);

	// Using FText because this is display text that should be localizable
	SpeedDisplayString = FText::Format(LOCTEXT("SpeedFormat", "{0} km/h"), FText::AsNumber(KPH_int));

	if (bInReverseGear == true)
	{
		GearDisplayString = FText(LOCTEXT("ReverseGear", "R"));
	}
	else
	{
		int32 Gear = GetVehicleMovement()->GetCurrentGear();
		GearDisplayString = (Gear == 0) ? LOCTEXT("N", "N") : FText::AsNumber(Gear);
	}
}

void AMMOPawn::SetupInCarHUD()
{
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if ((PlayerController != nullptr) && (InCarSpeed != nullptr) && (InCarGear != nullptr))
	{
		// Setup the text render component strings
		InCarSpeed->SetText(SpeedDisplayString);
		InCarGear->SetText(GearDisplayString);

		if (bInReverseGear == false)
		{
			InCarGear->SetTextRenderColor(GearDisplayColor);
		}
		else
		{
			InCarGear->SetTextRenderColor(GearDisplayReverseColor);
		}
	}
}


void AMMOPawn::postDataHTTP()
{
	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	Request->SetURL("http://192.168.1.2/unreal/postData.php");
	Request->OnProcessRequestComplete().BindUObject(this, &AMMOPawn::onDataPosted);
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));

	// Modify player location to increase by 1

//	FVector actorLocation = GetActorLocation();

	FRotator actorRotation = GetActorRotation();

	// Send data as is, only increase X by 1
	FString json = "{ \"slotID\" : " + FString::FromInt(slotID) +
		", \"pUID\" : " + FString::FromInt(this->pUID) +
		", \"X\" : " + FString::SanitizeFloat(GetActorLocation().X + 1.0f) +
		", \"Y\" : " + FString::SanitizeFloat(GetActorLocation().Y) +
		", \"Z\" : " + FString::SanitizeFloat(GetActorLocation().Z) +
		", \"Pitch\" : " + FString::SanitizeFloat(actorRotation.Pitch) +
		", \"Yaw\" : " + FString::SanitizeFloat(actorRotation.Yaw) +
		", \"Roll\" : " + FString::SanitizeFloat(actorRotation.Roll) +
		// This should post only once. Move it from here.
		", \"M\" : " + FString::FromInt(this->materialIndex) +
		"}";

	Request->SetContentAsString(json);
	Request->ProcessRequest();
}


void AMMOPawn::onDataPosted(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	logRequest(bWasSuccessful);
}

void AMMOPawn::getDataHTTP(int32 ID)
{
	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AMMOPawn::onDataReceived);
	Request->SetURL("http://192.168.1.2/unreal/getData.php");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	FString json = "{ \"slotID\" : " + FString::FromInt(ID) + "}";
	Request->SetContentAsString(json);
	Request->ProcessRequest();
}

void AMMOPawn::onDataReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	logRequest(bWasSuccessful);

	if (bWasSuccessful) {
		// Request was successful
		// Increase X by one
	/*	FVector actorLocation = GetActorLocation();
		actorLocation.X += 1.0f;
		SetActorLocation(actorLocation, false, NULL, ETeleportType::TeleportPhysics);*/
		//		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Data received for pUID: ") + FString::FromInt(pUID));

		try {

			//Create a pointer to hold the json serialized data
			TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject);

			//Create a reader pointer to read the json data
			TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

			//Deserialize the json data given Reader and the actual object to deserialize
			if (FJsonSerializer::Deserialize(Reader, JsonObject))
			{

				TSharedPtr<FJsonObject> playerData = JsonObject->GetObjectField("playerData");

				FVector location(playerData->GetNumberField("X"), playerData->GetNumberField("Y"), playerData->GetNumberField("Z"));
				FRotator rotation(playerData->GetNumberField("Pitch"), playerData->GetNumberField("Yaw"), playerData->GetNumberField("Roll"));

				this->SetActorLocation(location, false, NULL, ETeleportType::TeleportPhysics);
				this->SetActorRotation(rotation, ETeleportType::TeleportPhysics);
			}

		}
		catch (...) {
			// Show debug error
		}
	}
	else {
		//		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Failed Requests ") + FString::FromInt(pUID));
		//		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Failed to get data, pUID: ") + FString::FromInt(pUID));
	}


}

void AMMOPawn::inactivateSlot(int32 ID)
{
	TSharedRef<IHttpRequest> Request = http->CreateRequest();
	//	Request->OnProcessRequestComplete().BindUObject(this, &AMMOPawn::onDataReceived);
	Request->SetURL("http://192.168.1.2/unreal/setInactive.php");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	FString json = "{ \"slotID\" : " + FString::FromInt(ID) + "}";
	Request->SetContentAsString(json);
	Request->ProcessRequest();
}

void AMMOPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (isActive) {
		this->inactivateSlot(this->slotID);
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Orange, TEXT("Game has ended!"));
	}
}

float AMMOPawn::randomFloat(float a, float b)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;

}

void AMMOPawn::loadMaterial()
{
	// Get random material and assign to vehicle to change color
	if (GetMesh() != NULL) {
		UMaterialInstanceDynamic* myMaterial = UMaterialInstanceDynamic::Create(GetMesh()->GetMaterial(this->materialIndex), this);

		if (myMaterial != NULL)
		{
			// Not working
			//			myMaterial->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0.0f, 0.0f, 0.0f));

			GetMesh()->SetMaterial(2, myMaterial); // 2 is the index that seems to change vehicle color
		}
	}
}

void AMMOPawn::failedRequest()
{
	failedRequests++;
	if (DEBUG) {
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Orange, TEXT("Failed Requests: ") + FString::FromInt(failedRequests));
	}
}

void AMMOPawn::successfulRequest()
{
	successfulRequests++;
	if (DEBUG) {
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Orange, TEXT("Successful Requests: ") + FString::FromInt(successfulRequests));
	}
}

void AMMOPawn::logRequest(bool bWasSuccessful)
{
	bWasSuccessful ? successfulRequest() : failedRequest();
}



#undef LOCTEXT_NAMESPACE
