// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "MMOPawn.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include <vector>
#include "MMOGameMode.generated.h"

class AMMOPawn;

UCLASS(minimalapi)
class AMMOGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	

	FHttpModule* http;
	int32 numberOfActivePlayers = 0;

	TMap<int32, AMMOPawn*> players;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "References")
		AMMOPawn* vehiclePawn;

	AMMOGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	int32 loadSavedUID();

	// Get first empty slot. Logic implemented on server
	void getSlot();
	void onSlotReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Get player UID from remote server
	void downloadUID();
	void onUIDReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Confirm received player UID with server
	void confirmUID(int32 UID);
	void onUIDConfirmed(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Save UID in .sav file
	void saveUID(int32 UID);

	// Get list of currently active players
	void downloadActivePlayers(int32 ID);
	void onActivePlayersReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Spawn new player in scene
	AMMOPawn* addPlayer(int32 slotID, int32 materialIndex, int32 playerUID, bool isActive = false, bool isOtherPlayer = true);

	void processPlayers(TMap<int32, TTuple<int32, int32>> map);

	
};


