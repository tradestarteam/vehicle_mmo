// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MMOSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class MMO_API UMMOSaveGame : public USaveGame
{
	GENERATED_BODY()
	
	
public:

	UPROPERTY(VisibleAnywhere, Category = Basic)
		int32 savedUID;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString slotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		int32 userIndex;

	UMMOSaveGame();
	
};
