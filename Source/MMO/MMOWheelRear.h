// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "MMOWheelRear.generated.h"

UCLASS()
class UMMOWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UMMOWheelRear();
};



